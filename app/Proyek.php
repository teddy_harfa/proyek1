<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Proyek extends Model
{
    //
    protected $table='proyek';
    protected $fillable = [
        'proyek_id',
        'mhs_nim',
        'proyek_nama',
        'proyek_deadline',
        'proyek_detail',
        'proyek_status'
    ];
}
