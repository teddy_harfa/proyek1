<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Komentar extends Model
{
    //
    protected $table='komentar';
    protected $fillable = [
        'mhs_nim',
        'dp_id',
        'komen_isi'
    ];
}
