<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tugas extends Model
{
    //
    protected $table='detail_proyek';
    protected $fillable = [
        'dp_id',
        'mhs_nim',
        'proyek_id',
        'dp_tugas',
        'dp_detail_tugas',
        'dp_deadine',
        'dp_file',
        'dp_progress'
    ];
}
