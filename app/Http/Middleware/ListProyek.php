<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;

class ListProyek
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $lproyek = json_decode(DB::table('anggota_proyek')
        ->join('proyek','anggota_proyek.proyek_id','=','proyek.proyek_id')
        ->selectraw('proyek.proyek_id,proyek.proyek_nama,proyek.proyek_status')
        ->where('anggota_proyek.mhs_nim','=',session('nim'))
        ->get(),true);
        View::share('lproyek', $lproyek);
        return $next($request);
    }
}
