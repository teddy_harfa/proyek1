<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('listproyek');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $nim = session('nim');
        $home = array();
        $home['proyek']=\App\AnggotaP::where('mhs_nim',$nim)->get()->count();
        $home['tugas']=\App\Tugas::where('mhs_nim',$nim)->where('dp_progress','!=','100')->get()->count();
        $home['selesai']=\App\Proyek::where('mhs_nim',$nim)->where('proyek_status','Selesai')->get()->count();
        $home['batal']=\App\Proyek::where('mhs_nim',$nim)->where('proyek_status','Dibatalkan')->get()->count();
        return view('data.index',compact('home'));
    }
}
