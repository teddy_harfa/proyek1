<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ProyekController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('listproyek');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($nim)
    {
        //
        $proyek = json_decode(DB::table('anggota_proyek')
        ->join('proyek','anggota_proyek.proyek_id','=','proyek.proyek_id')
        ->join('users','anggota_proyek.mhs_nim','=','users.mhs_nim')
        ->selectraw('proyek.proyek_id,users.mhs_first_name,users.mhs_last_name,proyek.proyek_nama,proyek.proyek_deadline,proyek.proyek_status')
        ->where('anggota_proyek.mhs_nim','=',$nim)
        ->get(),true);
        // $proyek = \App\Proyek::where('mhs_nim',$nim)->get();
        return view("data.list-proyek",compact('proyek'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('data.tambah-proyek');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $validatedData = $request->validate([
            'proyek_nama' => 'required',
            'proyek_deadline' => 'required'
        ],
        [
            'proyek_nama.required' => 'Nama Proyek harus diisi',
            'proyek_deadline.required' => 'Deadline Proyek harus diisi'
        ]);
        $proyek = new \App\Proyek();
        $proyek->mhs_nim = session('nim');
        $proyek->proyek_nama = $request->input('proyek_nama');
        $proyek->proyek_deadline = $request->input('proyek_deadline');
        $proyek->proyek_detail = $request->input('proyek_detail');
        $proyek->proyek_status = "Berjalan";
        $proyek->save();
        $anggotap = new \App\AnggotaP();
        $anggotap->proyek_id = $proyek->id;
        $anggotap->mhs_nim = session('nim');
        $anggotap->save();
        return redirect()->route('list-proyek',['nim'=>session('nim')])->with('info','Proyek ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $proyek = \App\Proyek::where('proyek_id',$id)->first();
        $ketua = \App\User::where('mhs_nim',$proyek['mhs_nim'])->first();
        $anggotap = json_decode(DB::table('anggota_proyek')
        ->join('users','anggota_proyek.mhs_nim','=','users.mhs_nim')
        ->selectraw('anggota_proyek.mhs_nim,users.mhs_first_name,users.mhs_last_name')
        ->where('anggota_proyek.proyek_id','=',$id)
        ->get(),true);
        $tugas = json_decode(DB::table('detail_proyek')
        ->join('users','detail_proyek.mhs_nim','=','users.mhs_nim')
        ->selectraw('detail_proyek.dp_id,detail_proyek.proyek_id,detail_proyek.dp_tugas,
        detail_proyek.dp_detail_tugas,detail_proyek.dp_deadline,detail_proyek.dp_file,
        detail_proyek.dp_progress,users.mhs_first_name,users.mhs_last_name')
        ->where('detail_proyek.proyek_id','=',$id)
        ->get(),true);
        return view('data.proyek',compact('proyek','ketua','anggotap','tugas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $proyek = \App\Proyek::where('proyek_id',$id)->first();
        return view('data.kelola-proyek',compact('proyek'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData = $request->validate([
            'proyek_nama' => 'required',
            'proyek_deadline' => 'required'
        ],
        [
            'proyek_nama.required' => 'Nama Proyek harus diisi',
            'proyek_deadline.required' => 'Deadline Proyek harus diisi'
        ]);
        \App\Proyek::where('proyek_id',$id)->update([
            'proyek_nama' => $request->input('proyek_nama'),
            'proyek_deadline' => $request->input('proyek_deadline'),
            'proyek_detail' => $request->input('proyek_detail')
        ]);
        return redirect()->route('proyek',['id'=>$id])->with('info','Proyek diedit!');
    }

    public function status(Request $request, $id)
    {
        //
        if($request->input('status')=='Selesai'){
            $tugas = \App\Tugas::where('proyek_id',$id)->get();
            $selesai = true;
            foreach($tugas as $t){
                if($t['dp_progress']<100){
                    $selesai = false;
                }
            }
            if($selesai){
                \App\Proyek::where('proyek_id',$id)->update([
                    'proyek_status' => $request->input('status')
                ]);
            }else{
                return redirect()->route('proyek',['id'=>$id])->with('info','Tugas Belum Selesai!');
            }
        }else{
            \App\Proyek::where('proyek_id',$id)->update([
                'proyek_status' => $request->input('status')
            ]);
        }
        return redirect()->route('proyek',['id'=>$id])->with('info','Status Proyek diubah!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        
    }
}
