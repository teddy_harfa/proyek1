<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Storage;

class TugasController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('listproyek');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        //
        $tugas = json_decode(DB::table('detail_proyek')
        ->join('users','detail_proyek.mhs_nim','=','users.mhs_nim')
        ->selectraw('detail_proyek.dp_id,detail_proyek.proyek_id,detail_proyek.dp_tugas,
        detail_proyek.dp_detail_tugas,detail_proyek.dp_deadline,detail_proyek.dp_file,
        detail_proyek.dp_progress,users.mhs_first_name,users.mhs_last_name')
        ->where('detail_proyek.proyek_id','=',$id)
        ->get(),true);
        $proyek_id = $id;
        return view('data.kelola-tugas',compact('tugas','proyek_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        //
        $anggotap = json_decode(DB::table('anggota_proyek')
        ->join('users','anggota_proyek.mhs_nim','=','users.mhs_nim')
        ->selectraw('anggota_proyek.mhs_nim,users.mhs_first_name,users.mhs_last_name')
        ->where('anggota_proyek.proyek_id','=',$id)
        ->get(),true);
        $proyek_id = $id;
        return view('data.tambah-tugas',compact('anggotap','proyek_id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$id)
    {
        //
        $validatedData = $request->validate([
            'dp_tugas' => 'required',
            'dp_deadline' => 'required'
        ],
        [
            'dp_tugas.required' => 'Nama Tugas harus diisi',
            'dp_deadline.required' => 'Deadline Tugas harus diisi'
        ]);
        $tugas = new \App\Tugas();
        $tugas->mhs_nim=$request->input('mhs_nim');
        $tugas->proyek_id=$id;
        $tugas->dp_tugas=$request->input('dp_tugas');
        $tugas->dp_detail_tugas=$request->input('dp_detail_tugas');
        $tugas->dp_deadline=$request->input('dp_deadline');
        $tugas->dp_file="";
        $tugas->dp_progress=0;
        $tugas->save();
        return redirect()->route('kelola-tugas',['id'=>$id])->with('info','Tugas ditambahkan!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $tugas = json_decode(DB::table('detail_proyek')
        ->join('users','detail_proyek.mhs_nim','=','users.mhs_nim')
        ->join('proyek','proyek.proyek_id','=','detail_proyek.proyek_id')
        ->selectraw('detail_proyek.dp_id,detail_proyek.proyek_id,proyek.mhs_nim as ketua_nim,detail_proyek.dp_tugas,
        detail_proyek.dp_detail_tugas,detail_proyek.dp_deadline,detail_proyek.dp_file,
        detail_proyek.dp_progress,detail_proyek.mhs_nim,users.mhs_first_name,users.mhs_last_name')
        ->where('detail_proyek.dp_id','=',$id)
        ->get(),true);
        $komentar = json_decode(DB::table('komentar')
        ->join('users','komentar.mhs_nim','=','users.mhs_nim')
        ->selectraw('komentar.mhs_nim,users.mhs_first_name,users.mhs_last_name,komentar.komen_isi,komentar.created_at')
        ->where('komentar.dp_id','=',$id)
        ->get(),true);
        return view('data.detail-tugas',compact('tugas','komentar'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $tugas = \App\Tugas::where('dp_id',$id)->first();
        $proyek_id = $tugas['proyek_id'];
        $anggotap = json_decode(DB::table('anggota_proyek')
        ->join('users','anggota_proyek.mhs_nim','=','users.mhs_nim')
        ->selectraw('anggota_proyek.mhs_nim,users.mhs_first_name,users.mhs_last_name')
        ->where('anggota_proyek.proyek_id','=',$proyek_id)
        ->get(),true);
        return view('data.edit-tugas',compact('anggotap','tugas'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $validatedData = $request->validate([
            'dp_tugas' => 'required',
            'dp_deadline' => 'required'
        ],
        [
            'dp_tugas.required' => 'Nama Tugas harus diisi',
            'dp_deadline.required' => 'Deadline Tugas harus diisi'
        ]);
        \App\Tugas::where('dp_id',$id)->update([
            'mhs_nim'=>$request->input('mhs_nim'),
            'dp_tugas'=>$request->input('dp_tugas'),
            'dp_detail_tugas'=>$request->input('dp_detail_tugas'),
            'dp_deadline'=>$request->input('dp_deadline')
        ]);
        $tugas = \App\Tugas::where('dp_id',$id)->first();
        $proyek_id = $tugas['proyek_id'];
        return redirect()->route('kelola-tugas',['id'=>$proyek_id])->with('info','Tugas diedit!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $tugas = \App\Tugas::where('dp_id',$id)->first();
        //delete if file exists
        $exist = Storage::disk('file')->exists($tugas['dp_file']);
        if(isset($tugas['dp_file']) && $exist){
            $delete = Storage::disk('file')->delete($tugas['dp_file']);
        }
        $proyek_id = $tugas['proyek_id'];
        \App\Tugas::where('dp_id',$id)->delete();
        return redirect()->route('kelola-tugas',['id'=>$proyek_id])->with('info','Tugas dihapus!');
    }

    public function komentar(Request $request,$id)
    {
        //
        $validatedData = $request->validate([
            'komentar' => 'required'
        ],
        [
            'komentar.required' => 'Isi Komentar harus diisi'
        ]);
        $komentar = new \App\Komentar();
        $komentar->mhs_nim=session('nim');
        $komentar->dp_id=$id;
        $komentar->komen_isi=$request->input('komentar');
        $komentar->save();
        return redirect()->route('detail-tugas',['id'=>$id])->with('info','Komentar ditambahkan!');
    }

    public function progress(Request $request,$id)
    {
        $tugas = \App\Tugas::where('dp_id',$id)->first();
        if ($tugas['dp_progress']==0) {
            \App\Tugas::where('dp_id',$id)->update([
                'dp_progress'=>25
            ]);
        }
        elseif ($tugas['dp_progress']==25) {
            $validatedData = $request->validate([
                'dp_file' => 'required|max:2000'
            ],
            [
                'dp_file.required' => 'File tugas harus diupload',
                'dp_file.max' => 'Batas maksimum file yang dapat diupload 2000KB'
            ]);
            if($request->hasFile('dp_file')){
                $file = $request->file('dp_file')->getClientOriginalName();
                //get filename
                $filename = pathinfo($file,PATHINFO_FILENAME);
                //get extension
                $ext = $request->file('dp_file')->getClientOriginalExtension();
                if($request->file('dp_file')->isValid()){
                    $fileNametoStore= $filename.'_'.time().'.'.$ext;
                    $upload_path = 'file';
                    $request->file('dp_file')->move($upload_path,$fileNametoStore);    
                }
            }
            if ($request->input('result')==0){
                \App\Tugas::where('dp_id',$id)->update([
                    'dp_progress'=>50,
                    'dp_file'=>$fileNametoStore
                ]);
            }
            else{
                \App\Tugas::where('dp_id',$id)->update([
                    'dp_progress'=>75,
                    'dp_file'=>$fileNametoStore
                ]);
            }
        }
        elseif ($tugas['dp_progress']==50) {
            $validatedData = $request->validate([
                'dp_file' => 'required|max:2000'
            ],
            [
                'dp_file.required' => 'File tugas harus diupload',
                'dp_file.max' => 'Batas maksimum file yang dapat diupload 2000KB'
            ]);
            if($request->hasFile('dp_file')){
                $tugas = \App\Tugas::where('dp_id',$id)->first();
                //delete if file exists
                $exist = Storage::disk('file')->exists($tugas['dp_file']);
                if(isset($tugas['dp_file']) && $exist){
                    $delete = Storage::disk('file')->delete($tugas['dp_file']);
                }

                $file = $request->file('dp_file')->getClientOriginalName();
                //get filename
                $filename = pathinfo($file,PATHINFO_FILENAME);
                //get extension
                $ext = $request->file('dp_file')->getClientOriginalExtension();
                if($request->file('dp_file')->isValid()){
                    $fileNametoStore= $filename.'_'.time().'.'.$ext;
                    $upload_path = 'file';
                    $request->file('dp_file')->move($upload_path,$fileNametoStore);    
                }
            }
            \App\Tugas::where('dp_id',$id)->update([
                'dp_progress'=>75,
                'dp_file'=>$fileNametoStore
            ]);
        }
        elseif ($tugas['dp_progress']==75 || $tugas['dp_progress']==95) {
            if ($request->input('result')==0){
                \App\Tugas::where('dp_id',$id)->update([
                    'dp_progress'=>90
                ]);
            }
            else{
                \App\Tugas::where('dp_id',$id)->update([
                    'dp_progress'=>100
                ]);
            }
        }
        elseif ($tugas['dp_progress']==90) {
            $validatedData = $request->validate([
                'dp_file' => 'required|max:2000'
            ],
            [
                'dp_file.required' => 'File tugas harus diupload',
                'dp_file.max' => 'Batas maksimum file yang dapat diupload 2000KB'
            ]);
            if($request->hasFile('dp_file')){
                $tugas = \App\Tugas::where('dp_id',$id)->first();
                //delete if file exists
                $exist = Storage::disk('file')->exists($tugas['dp_file']);
                if(isset($tugas['dp_file']) && $exist){
                    $delete = Storage::disk('file')->delete($tugas['dp_file']);
                }

                $file = $request->file('dp_file')->getClientOriginalName();
                //get filename
                $filename = pathinfo($file,PATHINFO_FILENAME);
                //get extension
                $ext = $request->file('dp_file')->getClientOriginalExtension();
                if($request->file('dp_file')->isValid()){
                    $fileNametoStore= $filename.'_'.time().'.'.$ext;
                    $upload_path = 'file';
                    $request->file('dp_file')->move($upload_path,$fileNametoStore);    
                }
            }
            \App\Tugas::where('dp_id',$id)->update([
                'dp_progress'=>95,
                'dp_file'=>$fileNametoStore
            ]);
        }
        return redirect()->route('detail-tugas',['id'=>$id])->with('info','Progress Pekerjaan diupdate!');
    }

    public function tugas($id)
    {
        $tugas = json_decode(DB::table('detail_proyek')
        ->join('users','detail_proyek.mhs_nim','=','users.mhs_nim')
        ->join('proyek','proyek.proyek_id','=','detail_proyek.proyek_id')
        ->selectraw('detail_proyek.dp_id,detail_proyek.proyek_id,proyek.mhs_nim as ketua_nim,detail_proyek.dp_tugas,
        detail_proyek.dp_detail_tugas,detail_proyek.dp_deadline,detail_proyek.dp_file,
        detail_proyek.dp_progress,detail_proyek.mhs_nim,users.mhs_first_name,users.mhs_last_name')
        ->where('detail_proyek.dp_id','=',$id)
        ->get(),true);
        return view('data.tugas',compact('tugas'));
    }
}
