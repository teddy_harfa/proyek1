<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AnggotaPController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('listproyek');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $proyek = \App\Proyek::where('proyek_id',$id)->first();
        $anggotap = json_decode(DB::table('anggota_proyek')
        ->join('users','anggota_proyek.mhs_nim','=','users.mhs_nim')
        ->selectraw('anggota_proyek.mhs_nim,users.mhs_first_name,users.mhs_last_name')
        ->where('anggota_proyek.proyek_id','=',$id)
        ->get(),true);
        return view('data.kelola-anggota',compact('anggotap','proyek'));
    }

    public function search($id,Request $request)
    {
        //
        $validatedData = $request->validate([
            'nim' => 'required'
        ],
        [
            'nim.required' => 'NIM mahasiswa harus diisi'
        ]);
        $proyek = \App\Proyek::where('proyek_id',$id)->first();
        $anggotap = \App\AnggotaP::where('proyek_id',$id)->get();
        $nim = $request->input('nim');
        $search = \App\User::where('mhs_nim',$nim)->get();
        return view('data.kelola-anggota',compact('search','anggotap','proyek'));
    }

    public function insert($id,$nim)
    {
        //
        $anggotap = new \App\AnggotaP();
        $anggotap->proyek_id = $id;
        $anggotap->mhs_nim = $nim;
        $anggotap->save();
        return redirect()->route('kelola-anggota',$id)->with('info','Anggota ditambahkan');
    }

    public function delete($id,$nim)
    {
        //
        \App\AnggotaP::where('proyek_id',$id)->where('mhs_nim',$nim)->delete();
        return redirect()->route('kelola-anggota',$id)->with('info','Anggota dihapus');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
