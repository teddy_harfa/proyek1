<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AnggotaP extends Model
{
    //
    protected $table='anggota_proyek';
    protected $fillable = [
        'proyek_id',
        'mhs_nim'
    ];
}
