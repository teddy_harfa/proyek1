<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mahasiswa extends Model
{
    //
    protected $table='mahasiswa';
    protected $primaryKey = 'mhs_nim';
    public $incrementing = false;
    protected $fillable = [
        'mhs_nim',
        'mhs_password',
        'mhs_first_name',
        'mhs_last_name',
        'mhs_email',
        'mhs_kelas'
    ];
}
