<div class="app-header header-shadow">
        <div class="app-header__logo">
            <div class="logo-src"></div>
            <div class="header__pane ml-auto">
                <div>
                    <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
        </div>
        <div class="app-header__mobile-menu">
            <div>
                <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
        <div class="app-header__menu">
            <span>
                <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                    <span class="btn-icon-wrapper">
                        <i class="fa fa-ellipsis-v fa-w-6"></i>
                    </span>
                </button>
            </span>
        </div>    <div class="app-header__content">
            <div class="app-header-right">
                <div class="header-btn-lg pr-0">
                    <div class="widget-content p-0">
                        <div class="widget-content-wrapper">
                            <div class="widget-content-left">
                                <div class="btn-group">
                                    {{-- @if (Auth::guest())
                                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                        <img width="42" class="rounded-circle" src="assets/images/avatars/user.png" alt="guest image">
                                        <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                    </a>
                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                        <h6 tabindex="-1" class="dropdown-header">Tentang Situs</h6>
                                        <a class="dropdown-item" href="{{url('about')}}"><i class="fa fa-info-circle"></i>&nbsp; About</a>
                                        <div tabindex="-1" class="dropdown-divider"></div>
                                        <h6 tabindex="-1" class="dropdown-header">Autentikasi</h6>
                                        <a class="dropdown-item" href="{{ route('login') }}"><i class="fa fa-key"></i>&nbsp; {{ __('Login') }}</a>
                                        @if (Route::has('register'))
                                        <a class="dropdown-item" href="{{ route('register') }}"><i class="fa fa-user-plus"></i>&nbsp; {{ __('Register') }}</a>
                                        @endif
                                    </div>
                                @else --}}
                                    <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                    <img width="42" class="rounded-circle" src="{{asset('assets/images/avatars/admin.png')}}" alt="admin image">
                                        <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                    </a>
                                    <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                    <h6 tabindex="-1" class="dropdown-header">Tentang Situs</h6>
                                    <a class="dropdown-item" href="{{url('about')}}"><i class="fa fa-info-circle"></i>&nbsp; About</a>
                                        <div tabindex="-1" class="dropdown-divider"></div>
                                        <h6 tabindex="-1" class="dropdown-header">Keluar</h6>
                                        <a class="dropdown-item" href="#
                                        {{ route('logout') }}
                                        "
                                            onclick="event.preventDefault();
                                                          document.getElementById('logout-form').submit();">
                                             <i class="fa fa-power-off"></i>&nbsp; {{ __('Logout') }}
                                         </a>
     
                                         <form id="logout-form" action="
                                         {{ route('logout') }}
                                         " method="POST" style="display: none;">
                                             @csrf
                                         </form>
                                    </div>
                                {{-- @endif --}}
                                </div>
                            </div>
                            <div class="widget-content-left  ml-3 header-user-info">
                                {{-- @if (Auth::guest()) --}}
                                <div class="widget-heading">
                                    {{session('nim')}}
                                </div>
                                <div class="widget-subheading">
                                    {{session('first_name')}} {{session('last_name')}}
                                </div>
                                {{-- @else
                                <div class="widget-heading">
                                    {{ Auth::user()->name }}
                                </div>
                                <div class="widget-subheading">
                                    @if (Auth::user()->hasVerifiedEmail() == false)
                                    User 
                                    @else
                                    Admin
                                    @endif
                                </div>
                                @endif --}}
                            </div>
                            <div class="widget-content-right header-user-info ml-3">
                                <button type="button" class="btn-shadow p-1 btn btn-primary btn-sm show-toastr-example">
                                    <i class="fa text-white fa-calendar pr-1 pl-1"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>        </div>
        </div>
    </div>        