@extends('layouts.master')
@section('content')  
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-edit icon-gradient bg-ripe-malin">
                        </i>
                    </div>
                    <div>Edit Tugas
                        <div class="page-title-subheading">Edit data tugas
                        </div>
                    </div>
                </div>   
            </div>
        </div>   
        <div class="row">
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-body"><h5 class="card-title">Edit Tugas</h5>
                        <form action="{{url('updatetugas')}}/{{$tugas['dp_id']}}" method="POST">
                            @csrf
                            <div class="position-relative row form-group"><label for="dp_tugas" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10"><input name="dp_tugas" id="dp_tugas" placeholder="Nama Tugas" type="text" class="form-control @error('dp_tugas') is-invalid @enderror" value="{{$tugas['dp_tugas']}}">
                                    @error('dp_tugas')<div class="invalid-feedback">
                                        {{$message}}    
                                    </div>@enderror
                                </div>
                            </div>
                            <div class="position-relative row form-group"><label for="dp_detail_tugas" class="col-sm-2 col-form-label">Deskripsi</label>
                                <div class="col-sm-10"><textarea name="dp_detail_tugas" id="dp_detail_tugas" placeholder="Deksripsi Tugas" class="form-control" >{{$tugas['dp_detail_tugas']}}</textarea></div>
                            </div>
                            <div class="position-relative row form-group"><label for="mhs_nim" class="col-sm-2 col-form-label">Anggota</label>
                                <div class="col-sm-10">
                                    <select id="mhs_nim" name="mhs_nim" class="form-control">
                                        @foreach ($anggotap as $ap)
                                        <option value="{{$ap['mhs_nim']}}" {{$tugas['mhs_nim'] === $ap['mhs_nim'] ? "selected" : ""}}>{{$ap['mhs_first_name']}} {{$ap['mhs_last_name']}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="position-relative row form-group"><label for="dp_deadline" class="col-sm-2 col-form-label">Tanggal Deadline</label>
                                <div class="col-sm-10"><input name="dp_deadline" id="dp_deadline" placeholder="" type="date" class="form-control @error('dp_deadline') is-invalid @enderror" value="{{$tugas['dp_deadline']}}">
                                    @error('dp_deadline')<div class="invalid-feedback">
                                        {{$message}}    
                                    </div>@enderror
                                </div>
                            </div>
                            <button class="btn btn-primary" type="submit">Edit</button>
                        </form>
                    </div>
                </div>
            </div>    
        </div>
    </div>
@endsection