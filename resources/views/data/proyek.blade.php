@extends('layouts.master')
@section('content')    
				<div class="app-main__outer">
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>
                                        {{$proyek['proyek_nama']}}
                                        <div class="page-title-subheading">
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">
                                    @if ($proyek['mhs_nim']==session('nim') && $proyek['proyek_status']=='Berjalan')
                                    <div class="d-inline-block dropdown">
                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                                            <span class="btn-icon-wrapper pr-2 opacity-7">
                                                <i class="fa fa-business-time fa-w-20"></i>
                                            </span>
                                            Aksi
                                        </button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a href="{{url("kelola-proyek")}}/{{$proyek['proyek_id']}}" class="nav-link">
                                                        <i class="nav-link-icon lnr-inbox"></i>
                                                        <span>
                                                            Edit Detail Proyek
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{url("kelola-anggota")}}/{{$proyek['proyek_id']}}" class="nav-link">
                                                        <i class="nav-link-icon lnr-inbox"></i>
                                                        <span>
                                                            Kelola Anggota
                                                        </span>
                                                    </a>
                                                </li>
                                                <li class="nav-item">
                                                    <a href="{{url("kelola-tugas")}}/{{$proyek['proyek_id']}}" class="nav-link">
                                                        <i class="nav-link-icon lnr-inbox"></i>
                                                        <span>
                                                            Kelola Tugas
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>    
                                    @endif
                                    
                                </div>    </div>
                        </div>
                        <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                            <li class="nav-item">
                                <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                    <span>Detail</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                    <span>Anggota</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" id="tab-2" data-toggle="tab" href="#tab-content-2">
                                    <span>Tugas</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">            
                                <div class="row">
                                    {{-- @foreach ($p as $proyek) --}}
                                    <div class="col-lg-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">Detail Proyek</h5>
                                                <table class="mb-0 table">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Nama Proyek</td>
                                                        <td>{{$proyek['proyek_nama']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Deskripsi Proyek</td>
                                                        <td>{{$proyek['proyek_detail']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ketua</td>
                                                        <td>{{$ketua['mhs_first_name']}} {{$ketua['mhs_last_name']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Deadline</td>
                                                        <td>{{$proyek['proyek_deadline']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Status</td>
                                                        <td>{{$proyek['proyek_status']}}</td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-lg-6">
                                        <div class="main-card mb-3 card">
                                            @if ($proyek['mhs_nim']==session('nim'))
                                            <div class="card-body"><h5 class="card-title">Update Status Proyek</h5>
                                                {!! Form::open(['url'=>'status/'.$proyek['proyek_id']]) !!}
                                                <div class="row">
                                                    <div class="col-lg-8">
                                                        <select name="status" class="form-control-sm form-control">
                                                            <option value="Berjalan" {{$proyek['proyek_status']==='Berjalan'? 'selected=selected' : ''}}>Berjalan</option>
                                                            <option value="Selesai" {{$proyek['proyek_status']==='Selesai'? 'selected=selected' : ''}}>Selesai</option>
                                                            <option value="Dibatalkan" {{$proyek['proyek_status']==='Dibatalkan'? 'selected=selected' : ''}}>Dibatalkan</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-lg-4">
                                                        <button type="submit" class="btn btn-primary">Update Status</button>
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>    
                                            @else
                                            <div class="card-body"><h5 class="card-title">Status Proyek</h5>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <select name="status" class="form-control-sm form-control">
                                                            <option value="Berjalan" {{$proyek['proyek_status']==='Berjalan'? 'selected=selected' : ''}}>Berjalan</option>
                                                            <option value="Selesai" {{$proyek['proyek_status']==='Selesai'? 'selected=selected' : ''}}>Selesai</option>
                                                            <option value="Dibatalkan" {{$proyek['proyek_status']==='Dibatalkan'? 'selected=selected' : ''}}>Dibatalkan</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            @endif
                                        </div>
                                    </div>
                                    {{-- @endforeach --}}
                                </div>
                            </div>
                            <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body">
                                                <table class="wrap table">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>NIM</th>
                                                        <th>Nama Anggota</th>
                                                        <th>Role</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    
                                                    @foreach ($anggotap as $ap)
                                                    <tr>
                                                        <th scope="row">{{$loop->iteration}}</th>
                                                        <td>{{$ap['mhs_nim']}}</td>
                                                        <td>{{$ap['mhs_first_name']}} {{$ap['mhs_last_name']}}</td>
                                                        <td>{{$ap['mhs_nim']===$proyek['mhs_nim'] ? 'Ketua' : 'Anggota' }}</td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                   </div>
                            </div>
                            <div class="tab-pane tabs-animation fade" id="tab-content-2" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body">
                                                <table class="wrap table">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nama Tugas</th>
                                                        <th>Detail</th>
                                                        <th>Anggota</th>
                                                        <th>Progress</th>
                                                        <th>Tanggal Deadline</th>
                                                        <th>File</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        @foreach ($tugas as $t)
                                                        <th scope="row">{{$loop->iteration}}</th>
                                                        <td>{{$t['dp_tugas']}}</td>
                                                        <td>{{$t['dp_detail_tugas']}}</td>
                                                        <td>{{$t['mhs_first_name']}} {{$t['mhs_last_name']}}</td>
                                                        <td>{{$t['dp_progress']}}%</td>
                                                        <td>{{$t['dp_deadline']}}</td>
                                                        <td>
                                                            @if($t['dp_file']=="")
                                                            -
                                                            @else
                                                            <a href="{{url('file/'.$t['dp_file'])}}">{{$t['dp_tugas']}}</a>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            @if ($proyek['proyek_status']=='Berjalan')
                                                            <a href="{{url('detail-tugas')}}/{{$t['dp_id']}}" class="btn btn-primary">Detail</a>    
                                                            @else
                                                            <a href="#" class="btn btn-primary disabled">Detail</a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                   </div>
                            </div>
                        </div>
                    </div>
@endsection
