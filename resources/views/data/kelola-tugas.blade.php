@extends('layouts.master')
@section('content')    
				<div class="app-main__outer">
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>Kelola Tugas
                                        <div class="page-title-subheading">
                                            Tambah, edit atau hapus tugas
                                        </div>
                                    </div>
                                </div>
                                <div class="page-title-actions">
                                    <div class="d-inline-block dropdown">
                                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                                            <span class="btn-icon-wrapper pr-2 opacity-7">
                                                <i class="fa fa-business-time fa-w-20"></i>
                                            </span>
                                            Aksi
                                        </button>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            <ul class="nav flex-column">
                                                <li class="nav-item">
                                                    <a href="{{url("tambah-tugas")}}/{{$proyek_id}}" class="nav-link">
                                                        <i class="nav-link-icon lnr-inbox"></i>
                                                        <span>
                                                            Tambah Tugas
                                                        </span>
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                    </div>
                                </div>    </div>
                        </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div class="main-card mb-3 card">
                                        <div class="card-body"><h5 class="card-title">Kelola Tugas</h5>
                                            <table class="wrap table">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Nama Tugas</th>
                                                        <th>Detail</th>
                                                        <th>Anggota</th>
                                                        <th>Progress</th>
                                                        <th>Tanggal Deadline</th>
                                                        <th>File</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        @foreach ($tugas as $t)
                                                        <th scope="row">{{$loop->iteration}}</th>
                                                        <td>{{$t['dp_tugas']}}</td>
                                                        <td>{{$t['dp_detail_tugas']}}</td>
                                                        <td>{{$t['mhs_first_name']}} {{$t['mhs_last_name']}}</td>
                                                        <td>{{$t['dp_progress']}}</td>
                                                        <td>{{$t['dp_deadline']}}</td>
                                                        <td>
                                                            @if($t['dp_file']=="")
                                                            -
                                                            @else
                                                            <a href="{{url('file/'.$t['dp_file'])}}">{{$t['dp_tugas']}}</a>
                                                            @endif
                                                        </td>
                                                    <td>
                                                        <a href="{{url('edit-tugas')}}/{{$t['dp_id']}}" class="btn btn-warning">Edit</a>
                                                        <a href="{{url('hapus-tugas')}}/{{$t['dp_id']}}" class="btn btn-danger">Hapus</a>
                                                    </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
@endsection
