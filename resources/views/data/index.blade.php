@extends('layouts.master')
@section('content') 
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                        </i>
                    </div>
                    <div>Dashboard
                        <div class="page-title-subheading">
                            Selamat Datang {{session('first_name')}} {{session('last_name')}} ! 
                        </div>
                    </div>
                </div>   
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-midnight-bloom">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Banyak Proyek</div>
                            <div class="widget-subheading">Total semua proyek</div>
                        </div>
                        <div class="widget-content-right">
                        <div class="widget-numbers text-white"><span>{{$home['proyek']}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-arielle-smile">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Jumlah Tugas</div>
                            <div class="widget-subheading">Tugas yang sedang dikerjakan</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{$home['tugas']}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-grow-early">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Proyek Selesai</div>
                            <div class="widget-subheading">Jumlah proyek sukses dilaksanakan</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{$home['selesai']}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="d-xl-none d-lg-block col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-premium-dark">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Proyek Dibatalkan</div>
                            <div class="widget-subheading">Jumlah proyek yang batal</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-warning"><span>{{$home['batal']}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection