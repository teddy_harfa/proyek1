@extends('layouts.master')
@section('content')  
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-plus icon-gradient bg-ripe-malin">
                        </i>
                    </div>
                    <div>Tambah Proyek
                        <div class="page-title-subheading">Tambahkan proyek baru
                        </div>
                    </div>
                </div>    
            </div>
        </div>            
        <!-- <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
            <li class="nav-item">
            <a class="nav-link active" href="#">  -->
                <!-- list proyek -->
                    <!-- <span>< Kembali</span>
                </a>
            </li>
        </ul> -->
        <div class="row">
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-body"><h5 class="card-title">Tambah Proyek</h5>
                    <form class="" action="{{url('/storeproyek')}}" method="POST">
                            {{ csrf_field() }}
                            <div class="position-relative row form-group"><label for="proyek_nama" class="col-sm-2 col-form-label">Nama</label>
                                <div class="col-sm-10"><input name="proyek_nama" id="proyek_nama" placeholder="Nama Proyek" type="text" class="form-control @error('proyek_nama') is-invalid @enderror" value="{{old('proyek_nama')}}">
                                @error('proyek_nama')<div class="invalid-feedback">
                                    {{$message}}    
                                </div>@enderror
                                </div>
                            </div>
                            <div class="position-relative row form-group"><label for="proyek_deadline" class="col-sm-2 col-form-label">Tanggal Deadline</label>
                                <div class="col-sm-10"><input name="proyek_deadline" id="proyek_deadline" type="date" class="form-control @error('proyek_deadline') is-invalid @enderror" value="{{old('proyek_deadline')}}">
                                @error('proyek_deadline')<div class="invalid-feedback">
                                    {{$message}}    
                                </div>@enderror
                                </div>
                            </div>
                            <div class="position-relative row form-group"><label for="proyek_detail" class="col-sm-2 col-form-label">Deskripsi</label>
                                <div class="col-sm-10"><textarea name="proyek_detail" id="proyek_detail" class="form-control" placeholder="Deskripsi singkat tentang proyek">{{old('proyek_detail')}}</textarea></div>
                            </div>
                            <div class="position-relative row form-check">
                                <button class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
        </div>
    </div>
</div>
@endsection