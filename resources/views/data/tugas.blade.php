@extends('layouts.master')
@section('content')     
				<div class="app-main__outer">
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    @foreach ($tugas as $t)                                    
                                    <div>{{$t['dp_tugas']}}
                                        <div class="page-title-subheading">
                                            {{$t['dp_detail_tugas']}}
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>            
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body">
                                                <h5 class="card-title">
                                                    @if ($t['dp_progress']==25)
                                                        Kerjakan Tugas
                                                    @elseif ($t['dp_progress']==50)
                                                        Update Progress Tugas
                                                    @elseif ($t['dp_progress']==75)
                                                        Cek Tugas
                                                    @elseif ($t['dp_progress']==90)
                                                        Revisi Tugas
                                                    @elseif ($t['dp_progress']==95)
                                                        Cek Revisi
                                                    @endif    
                                                </h5>
                                                {!! Form::open(['url'=>'progress/'.$t['dp_id'],'files'=>true]) !!}
                                                    <div class="position-relative row form-group"><label for="dp_file" class="col-sm-2 col-form-label">File</label>
                                                        <div class="col-sm-10">
                                                            @if ($t['dp_progress']!=25)
                                                            <a href="{{url('file/'.$t['dp_file'])}}">{{$t['dp_tugas']}}</a>
                                                            @endif
                                                            @if ($t['dp_progress']==25 || $t['dp_progress']==50 || $t['dp_progress']==90)
                                                            <input name="dp_file" id="dp_file" type="file" class="form-control-file @error('dp_file') is-invalid @enderror">
                                                            @error('dp_file')<div class="invalid-feedback">
                                                                {{$message}}    
                                                            </div>@enderror
                                                            @endif
                                                        </div>
                                                    </div>
                                                        
                                                        @if ($t['dp_progress']==25)
                                                            <h5>Apakah File Sudah Fix?</h5>
                                                            <fieldset class="position-relative form-group">
                                                                <div class="position-relative form-check"><label class="form-check-label">
                                                                    <input name="result" type="radio" class="form-check-input" value="1">Ya</label>
                                                                </div>
                                                                <div class="position-relative form-check"><label class="form-check-label">
                                                                    <input name="result" type="radio" class="form-check-input" value="0">Tidak</label>
                                                                </div>
                                                            </fieldset>
                                                        @elseif ($t['dp_progress']==75)
                                                            <h5>Apakah Perlu Revisi?</h5>
                                                            <fieldset class="position-relative form-group">
                                                                <div class="position-relative form-check"><label class="form-check-label">
                                                                    <input name="result" type="radio" class="form-check-input" value="0">Ya</label>
                                                                </div>
                                                                <div class="position-relative form-check"><label class="form-check-label">
                                                                    <input name="result" type="radio" class="form-check-input" value="1">Tidak</label>
                                                                </div>
                                                            </fieldset>
                                                        @elseif ($t['dp_progress']==95)
                                                            <h5>Apakah Revisi Sudah Benar?</h5>
                                                            <fieldset class="position-relative form-group">
                                                                <div class="position-relative form-check"><label class="form-check-label">
                                                                    <input name="result" type="radio" class="form-check-input" value="1">Ya</label>
                                                                </div>
                                                                <div class="position-relative form-check"><label class="form-check-label">
                                                                    <input name="result" type="radio" class="form-check-input" value="0">Tidak</label>
                                                                </div>
                                                            </fieldset>
                                                        @else
                                                        {!! Form::hidden("result", "0") !!}
                                                        @endif
                                                        
                                                    <button type="submit" class="mt-1 btn btn-primary">Update Progress</button>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                @endforeach
                    </div>
@endsection