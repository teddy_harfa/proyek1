@extends('layouts.master')
@section('content')    
				<div class="app-main__outer">
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>Kelola Anggota
                                        <div class="page-title-subheading">
                                            Tambah atau Hapus anggota
                                        </div>
                                    </div>
                                </div>   
                            </div>
                        </div>
                        <ul class="body-tabs body-tabs-layout tabs-animated body-tabs-animated nav">
                            <li class="nav-item">
                                <a role="tab" class="nav-link active" id="tab-0" data-toggle="tab" href="#tab-content-0">
                                    <span>Tambah</span>
                                </a>
                            </li>
                            <li class="nav-item">
                                <a role="tab" class="nav-link" id="tab-1" data-toggle="tab" href="#tab-content-1">
                                    <span>Kelola</span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane tabs-animation fade show active" id="tab-content-0" role="tabpanel">            
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">Tambah Anggota</h5>
                                                <form class="form" action="{{url('carianggota')}}/{{$proyek['proyek_id']}}" method="POST">
                                                    @csrf
                                                    <div class="row">
                                                        <div class="col-lg-5">
                                                            <div class="position-relative form-group">
                                                                <label for="nim" class="sr-only">NIM</label>
                                                                <input name="nim"  id="nim" placeholder="Masukkan NIM anggota" value="{{old('nim')}}" type="text" class="form-control @error('nim') is-invalid @enderror">
                                                                @error('nim')<div class="invalid-feedback">
                                                                    {{$message}}    
                                                                </div>@enderror
                                                            </div>
                                                        </div>
                                                        <div class="col-lg-3">
                                                            <button class="btn btn-primary" type="submit">Cari</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>    
                                </div>
                                @isset($search)
                                @if ($search->isEmpty())
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">Hasil Pencarian</h5>
                                                <h4>Nim Tidak Ditemukan!</h4>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @else
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">Hasil Pencarian</h5>
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>NIM</th>
                                                        <th>Nama</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                        @foreach ($search as $s)
                                                        <tr>
                                                            <td>{{$s['mhs_nim']}}</td>
                                                            <td>{{$s['mhs_first_name']}} {{$s['mhs_last_name']}}</td>
                                                            <td><a href="{{url('tambahap')}}/{{$proyek['proyek_id']}}/{{$s['mhs_nim']}}" class="btn btn-primary">Tambah Anggota</a></td>
                                                        </tr>    
                                                        @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif    
                                @endisset
                                
                            </div>
                            <div class="tab-pane tabs-animation fade" id="tab-content-1" role="tabpanel">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">Kelola Anggota</h5>
                                                <table class="wrap table">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>NIM</th>
                                                        <th>Nama Anggota</th>
                                                        <th>Role</th>
                                                        <th>Aksi</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    @foreach ($anggotap as $ap)
                                                    <tr>
                                                        <th scope="row">{{$loop->iteration}}</th>
                                                        <td>{{$ap['mhs_nim']}}</td>
                                                        <td>{{$ap['mhs_first_name']}} {{$ap['mhs_last_name']}}</td>
                                                        <td>{{$ap['mhs_nim']===$proyek['mhs_nim'] ? 'Ketua' : 'Anggota' }}</td>
                                                        <td>
                                                            @if ($ap['mhs_nim']===$proyek['mhs_nim'])
                                                            -
                                                            @else
                                                            <a href="{{url('hapusap')}}/{{$proyek['proyek_id']}}/{{$ap['mhs_nim']}}" class="btn btn-danger">Hapus</a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                   </div>
                            </div>
                        </div>
                    </div>
@endsection
