@extends('layouts.master')
@section('content')    
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                        </i>
                    </div>
                    <div>List Proyek
                        <div class="page-title-subheading">Daftar proyek yang anda ikuti
                        </div>
                    </div>
                </div>
                <div class="page-title-actions">
                    
                    <div class="d-inline-block dropdown">
                        <button type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="btn-shadow dropdown-toggle btn btn-info">
                            <span class="btn-icon-wrapper pr-2 opacity-7">
                                <i class="fa fa-business-time fa-w-20"></i>
                            </span>
                            Aksi
                        </button>
                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                            <ul class="nav flex-column">
                                <li class="nav-item">
                                <a href="{{url("tambah-proyek")}}" class="nav-link">
                                        <i class="fa fa-plus"></i>
                                        <span>
                                            &nbsp;Tambah Proyek
                                        </span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>    </div>
        </div>            
        <div class="row">
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-body"><h5 class="card-title">List Proyek</h5>
                        <table class="mb-0 table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Nama Proyek</th>
                                <th>Ketua</th>
                                <th>Tanggal Deadline</th>
                                <th>Status</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($proyek as $p)
                                <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td>{{$p['proyek_nama']}}</td>
                                <td>{{$p['mhs_first_name']}} {{$p['mhs_last_name']}}</td>
                                <td>{{$p['proyek_deadline']}}</td>
                                <td>{{$p['proyek_status']}}</td>
                                <td><a class="btn btn-primary" href="{{url('proyek')}}/{{$p['proyek_id']}}">Menuju</a></td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection