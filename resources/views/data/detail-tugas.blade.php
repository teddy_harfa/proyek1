@extends('layouts.master')
@section('content')     
				<div class="app-main__outer">
                    <div class="app-main__inner">
                        <div class="app-page-title">
                            <div class="page-title-wrapper">
                                <div class="page-title-heading">
                                    <div class="page-title-icon">
                                        <i class="pe-7s-drawer icon-gradient bg-happy-itmeo">
                                        </i>
                                    </div>
                                    <div>Detail Tugas
                                        <div class="page-title-subheading">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>            
                                <div class="row">
                                    @foreach ($tugas as $t)
                                    <div class="col-lg-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">Detail Tugas</h5>
                                                <table class="mb-0 table">
                                                    <thead>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Nama Tugas</td>
                                                        <td>{{$t['dp_tugas']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Detail</td>
                                                        <td>{{$t['dp_detail_tugas']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Anggota</td>
                                                        <td>{{$t['mhs_first_name']}} {{$t['mhs_last_name']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Progress</td>
                                                        <td>
                                                            <div class="progress">
                                                                <div class="progress-bar" role="progressbar" aria-valuenow="{{$t['dp_progress']}}%" aria-valuemin="0" aria-valuemax="100" style="width: {{$t['dp_progress']}}%;"></div>
                                                            </div>
                                                            <div class="text-center">
                                                                @if ($t['dp_progress']==0)
                                                                    Belum Dikerjakan
                                                                @elseif ($t['dp_progress']==25)
                                                                    Sedang Dikerjakan
                                                                @elseif ($t['dp_progress']==50)
                                                                    Sebagian Dikerjakan
                                                                @elseif ($t['dp_progress']==75)
                                                                    Menunggu Pengecekan
                                                                @elseif ($t['dp_progress']==90)
                                                                    Proses Revisi
                                                                @elseif ($t['dp_progress']==95)
                                                                    Selesai Revisi
                                                                @else
                                                                    Selesai Dikerjakan
                                                                @endif    
                                                            </div>
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Tanggal Deadline</td>
                                                        <td>{{$t['dp_deadline']}}</td>
                                                    </tr>
                                                    <tr>
                                                        <td>File</td>
                                                        <td>
                                                            @if($t['dp_file']=="")
                                                            -
                                                            @else
                                                            <a href="{{url('file/'.$t['dp_file'])}}">{{$t['dp_tugas']}}</a>
                                                            @endif
                                                        </td>
                                                    </tr>
                                                    
                                                    </tbody>
                                                </table>
                                                @if ($t['mhs_nim']==session('nim'))
                                                    @if ($t['dp_progress']==0)
                                                    {!! Form::open(['url'=>'progress/'.$t['dp_id']]) !!}
                                                        <button class="mt-1 btn btn-success" type="submit">Kerjakan Tugas</button>
                                                    {!! Form::close() !!}
                                                    @elseif ($t['dp_progress']==25)
                                                        <a class="mt-1 btn btn-primary" href="{{url("tugas")}}/{{$t['dp_id']}}">Update Progress</a>
                                                    @elseif ($t['dp_progress']==50)
                                                        <a class="mt-1 btn btn-primary" href="{{url("tugas")}}/{{$t['dp_id']}}">Update Progress</a>
                                                    @elseif ($t['dp_progress']==90)
                                                        <a class="mt-1 btn btn-primary" href="{{url("tugas")}}/{{$t['dp_id']}}">Lakukan Revisi</a>
                                                    @endif
                                                @endif
                                                @if ($t['ketua_nim']==session('nim'))
                                                    @if ($t['dp_progress']==75)
                                                        <a class="mt-1 btn btn-primary" href="{{url("tugas")}}/{{$t['dp_id']}}">Cek Tugas</a>
                                                    @elseif ($t['dp_progress']==95)
                                                        <a class="mt-1 btn btn-primary" href="{{url("tugas")}}/{{$t['dp_id']}}">Cek Revisi</a>
                                                    @endif
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="main-card mb-3 card">
                                            <div class="card-body"><h5 class="card-title">Komentar</h5>
                                                <div class="row">
                                                    <div class="col-lg-12 mb-3">
                                                        <div class="overflow-auto border border-primary" style="height: 200px;">
                                                        @foreach ($komentar as $k)
                                                        [{{$k['created_at']}}] <span {{$k['mhs_nim']===$t['ketua_nim'] ? 'style=color:red;' : 'style=color:blue;'}}>{{$k['mhs_first_name']}} {{$k['mhs_last_name']}}</span> : {{$k['komen_isi']}}<br>
                                                        @endforeach
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <form action="{{url('komentar')}}/{{$t['dp_id']}}" method="POST">
                                                            @csrf
                                                        <div class="input-group">
                                                            <input type="text" name="komentar" placeholder="Beri Komentar..." class="form-control @error('komentar') is-invalid @enderror">
                                                            @error('komentar')<div class="invalid-feedback">
                                                                {{$message}}    
                                                            </div>@enderror
                                                            <div class="input-group-append">
                                                                <button type="submit" class="btn btn-primary">Kirim</button>
                                                            </div>
                                                        </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                    </div>
@endsection