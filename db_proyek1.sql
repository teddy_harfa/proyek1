-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2020 at 04:17 PM
-- Server version: 10.1.36-MariaDB
-- PHP Version: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_proyek1`
--
CREATE DATABASE IF NOT EXISTS `db_proyek1` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_proyek1`;

-- --------------------------------------------------------

--
-- Table structure for table `anggota_proyek`
--

DROP TABLE IF EXISTS `anggota_proyek`;
CREATE TABLE IF NOT EXISTS `anggota_proyek` (
  `proyek_id` int(11) NOT NULL,
  `mhs_nim` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `anggota_proyek`
--

TRUNCATE TABLE `anggota_proyek`;
--
-- Dumping data for table `anggota_proyek`
--

INSERT INTO `anggota_proyek` (`proyek_id`, `mhs_nim`, `created_at`, `updated_at`) VALUES
(1, 1931733084, '2020-04-23 23:17:42', '2020-04-23 23:17:42'),
(1, 1931733072, '2020-04-23 23:30:43', '2020-04-23 23:30:43'),
(1, 1931733077, '2020-04-23 23:50:14', '2020-04-23 23:50:14'),
(2, 1931733084, '2020-05-07 11:16:39', '2020-05-07 11:16:39'),
(2, 1931733077, '2020-05-07 11:17:04', '2020-05-07 11:17:04');

-- --------------------------------------------------------

--
-- Table structure for table `detail_proyek`
--

DROP TABLE IF EXISTS `detail_proyek`;
CREATE TABLE IF NOT EXISTS `detail_proyek` (
  `dp_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mhs_nim` int(11) NOT NULL,
  `proyek_id` int(11) NOT NULL,
  `dp_tugas` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dp_detail_tugas` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `dp_deadline` date NOT NULL,
  `dp_file` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `dp_progress` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`dp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `detail_proyek`
--

TRUNCATE TABLE `detail_proyek`;
--
-- Dumping data for table `detail_proyek`
--

INSERT INTO `detail_proyek` (`dp_id`, `mhs_nim`, `proyek_id`, `dp_tugas`, `dp_detail_tugas`, `dp_deadline`, `dp_file`, `dp_progress`, `created_at`, `updated_at`) VALUES
(3, 1931733084, 1, 'Tugas 1', 'Tugas Pertama', '2020-05-25', '', 0, '2020-05-03 01:32:09', '2020-05-07 10:10:01'),
(4, 1931733077, 1, 'Tambahan', 'Tugas Tambahan', '2020-05-26', '1_1588872492.docx', 100, '2020-05-07 09:55:16', '2020-05-07 10:30:03'),
(5, 1931733077, 2, 'Baru', 'b', '2020-05-11', '1_1588875703.docx', 100, '2020-05-07 11:17:37', '2020-05-07 11:22:04');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `failed_jobs`
--

TRUNCATE TABLE `failed_jobs`;
-- --------------------------------------------------------

--
-- Table structure for table `komentar`
--

DROP TABLE IF EXISTS `komentar`;
CREATE TABLE IF NOT EXISTS `komentar` (
  `mhs_nim` int(11) NOT NULL,
  `dp_id` int(11) NOT NULL,
  `komen_isi` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `komentar`
--

TRUNCATE TABLE `komentar`;
--
-- Dumping data for table `komentar`
--

INSERT INTO `komentar` (`mhs_nim`, `dp_id`, `komen_isi`, `created_at`, `updated_at`) VALUES
(1931733084, 2, 'Segera dikerjakan', '2020-04-23 23:52:11', '2020-04-23 23:52:11'),
(1931733077, 2, 'Baik', '2020-04-23 23:52:27', '2020-04-23 23:52:27'),
(1931733084, 4, 'Done,Good job', '2020-05-07 10:30:47', '2020-05-07 10:30:47'),
(1931733077, 4, 'Terima Kasih', '2020-05-07 10:31:01', '2020-05-07 10:31:01'),
(1931733084, 5, 'Mohon benahi filenya', '2020-05-07 11:21:26', '2020-05-07 11:21:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `migrations`
--

TRUNCATE TABLE `migrations`;
--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(9, '2014_10_12_000000_create_users_table', 1),
(17, '2014_10_12_100000_create_password_resets_table', 2),
(18, '2019_08_19_000000_create_failed_jobs_table', 2),
(19, '2020_04_02_070442_create_mahasiswa_table', 2),
(20, '2020_04_02_073135_create_proyek_table', 2),
(21, '2020_04_02_073925_create_anggota_proyek_table', 2),
(22, '2020_04_02_074330_create_detail_proyek_table', 2),
(23, '2020_04_02_075110_create_komentar_table', 2),
(27, '2020_04_22_075631_create_users_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `password_resets`
--

TRUNCATE TABLE `password_resets`;
-- --------------------------------------------------------

--
-- Table structure for table `proyek`
--

DROP TABLE IF EXISTS `proyek`;
CREATE TABLE IF NOT EXISTS `proyek` (
  `proyek_id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `mhs_nim` int(11) NOT NULL,
  `proyek_nama` varchar(30) COLLATE utf8mb4_unicode_ci NOT NULL,
  `proyek_deadline` date NOT NULL,
  `proyek_detail` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `proyek_status` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`proyek_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `proyek`
--

TRUNCATE TABLE `proyek`;
--
-- Dumping data for table `proyek`
--

INSERT INTO `proyek` (`proyek_id`, `mhs_nim`, `proyek_nama`, `proyek_deadline`, `proyek_detail`, `proyek_status`, `created_at`, `updated_at`) VALUES
(1, 1931733084, 'Baru', '2020-04-30', 'Proyek Baru', 'Berjalan', '2020-04-23 23:17:42', '2020-05-03 01:31:35'),
(2, 1931733084, 'New', '2020-05-16', 'new', 'Berjalan', '2020-05-07 11:16:39', '2020-05-07 11:23:03');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `mhs_nim` int(11) NOT NULL,
  `password` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mhs_first_name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mhs_last_name` varchar(15) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mhs_kelas` varchar(2) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Truncate table before insert `users`
--

TRUNCATE TABLE `users`;
--
-- Dumping data for table `users`
--

INSERT INTO `users` (`mhs_nim`, `password`, `mhs_first_name`, `mhs_last_name`, `email`, `mhs_kelas`, `remember_token`, `created_at`, `updated_at`) VALUES
(1931733084, '$2y$10$KjAvoEVbV7ZJHrjug7ws4.6Dtx1D9h4.aZQzxl3Z4z/UHspE674RG', 'Teddy', 'Harfa', 'teddyharfa@gmail.com', '2C', NULL, '2020-04-23 23:15:37', '2020-04-23 23:15:37'),
(1931733072, '$2y$10$rTGGAz1yNNc4LyxW34zDuOLkK82wiTIF2MsJatKMWbKru7W.DzGSK', 'Kukuh', 'Lutfi', 'kukuhlk@gmail.com', '2C', NULL, '2020-04-23 23:24:43', '2020-04-23 23:24:43'),
(1931733077, '$2y$10$M4yI4bs9rITvsXgMu19wMOSsrdJMQo57zRm7C5o8RLHGqnsjQIoIi', 'Ahmad', 'Yusuf', 'iqbal@gmail.com', '2C', NULL, '2020-04-23 23:48:36', '2020-04-23 23:48:36');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
