<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDetailProyekTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_proyek', function (Blueprint $table) {
            $table->increments('dp_id');
	        $table->integer('mhs_nim')->length(11);
		    $table->integer('proyek_id')->length(11);
            $table->string('dp_tugas',30);
            $table->text('dp_detail_tugas');
		    $table->date('dp_deadline');
            $table->string('dp_file',30);
		    $table->integer('dp_progress')->length(3);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_proyek');
    }
}
