<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('data.index');
});

Route::get('list-proyek/{nim}', 'ProyekController@index')->name('list-proyek');

Route::get('tambah-proyek', 'ProyekController@create');
Route::post('storeproyek', 'ProyekController@store');

Route::get('proyek/{id}', 'ProyekController@show')->name('proyek');
Route::get('kelola-proyek/{id}', 'ProyekController@edit');
Route::patch('editproyek/{id}', 'ProyekController@update');
Route::post('status/{id}', 'ProyekController@status');

Route::get('kelola-anggota/{id}', 'AnggotaPController@show')->name('kelola-anggota');
Route::post('carianggota/{id}', 'AnggotaPController@search');
Route::get('tambahap/{id}/{nim}','AnggotaPController@insert');
Route::get('hapusap/{id}/{nim}','AnggotaPController@delete');
Route::get('kelola-tugas/{id}', 'TugasController@index')->name('kelola-tugas');
Route::get('tambah-tugas/{id}', 'TugasController@create');
Route::post('storetugas/{id}', 'TugasController@store');
Route::get('edit-tugas/{id}', 'TugasController@edit');
Route::post('updatetugas/{id}', 'TugasController@update');
Route::get('hapus-tugas/{id}', 'TugasController@destroy');
Route::get('detail-tugas/{id}','TugasController@show')->name('detail-tugas');
Route::post('komentar/{id}', 'TugasController@komentar');
Route::get('tugas/{id}', 'TugasController@tugas');
Route::post('progress/{id}', 'TugasController@progress');
Route::get('detail-tugas', function () {
    return view('data.detail-tugas');
});

Route::get('tugas', function () {
    return view('data.tugas');
});

// route::get('login', function ()	{
// 	return view('login.login');
// });
// route::get('register', function ()	{
// 	return view('login.register');
// });
// route::get('forgotPass', function ()	{
// 	return view('login.forgotPass');
// });


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', function () {
    return view('welcome');
});




?>

